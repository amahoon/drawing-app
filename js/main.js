var canvas, canvasCTX, stage;
var drawingCanvas, crayonCanvas, erasingCanvas, clearCanvas;
var oldPt, oldMidPt;
var colour;
var stroke;
var pencil, crayon, eraser;
var sizeImg, sizeSelect;
var hide, show, options;

function init() {
    
    canvas = document.getElementById("drawCanvas");
    canvasCTX = canvas.getContext('2d');
    canvasCTX.globalCompositeOperation = "destination-over";

    clearCanvas = document.getElementById("clear");
    clearCanvas.addEventListener("click", clearAll, false);

    hide = document.getElementById("hide");
    hide.addEventListener("click", hideOpt, false);

    show = document.getElementById("show");
    show.addEventListener("click", showOpt, false);

    options = document.getElementById("options");

    sizeImg = document.querySelector("#sizeImg");
    crayonTextureImage = document.getElementById("texture");

    sizeChange(2);

    sizeSelect = document.getElementById("size");
    sizeSelect.addEventListener("change", function() {
    	sizeChange(this.value);
    })

    pencil = document.getElementById("draw");
    pencil.addEventListener("click", check, false);

    crayon = document.getElementById("crayon");
    crayon.addEventListener("click", check, false);

    eraser = document.getElementById("erase");
    eraser.addEventListener("click", check, false);

    stage = new createjs.Stage(canvas);
    stage.autoClear = false;
    stage.enableDOMEvents(true);
    check();
}

function check() {
	if(pencil.checked) {
		//console.log("pencil selected");

		createjs.Touch.enable(stage);
	    createjs.Ticker.setFPS(24);

	    drawingCanvas = new createjs.Shape();

	    stage.addEventListener("stagemousedown", drawMouseDown);
	    stage.addEventListener("stagemouseup", drawMouseUp);
	    stage.removeChild(crayonCanvas);
	    stage.removeEventListener("stagemousedown", crayonMouseDown);
	    stage.removeEventListener("stagemouseup", crayonMouseUp);
    	stage.removeChild(erasingCanvas);
    	stage.removeEventListener("stagemousedown", eraseMouseDown);
	    stage.removeEventListener("stagemouseup", eraseMouseUp);

	    stage.addChild(drawingCanvas);
	    stage.update();
	} 
	if(crayon.checked) {
		//console.log("crayon selected");

		createjs.Touch.enable(stage);
	    createjs.Ticker.setFPS(24);

	    crayonCanvas = new createjs.Shape();

	    stage.addEventListener("stagemousedown", crayonMouseDown);
	    stage.addEventListener("stagemouseup", crayonMouseUp);
    	stage.removeChild(drawingCanvas);
    	stage.removeEventListener("stagemousedown", drawMouseDown);
	    stage.removeEventListener("stagemouseup", drawMouseUp);
	    stage.removeChild(erasingCanvas);
    	stage.removeEventListener("stagemousedown", eraseMouseDown);
	    stage.removeEventListener("stagemouseup", eraseMouseUp);

	    stage.addChild(crayonCanvas);
	    stage.update();
	}
	if(eraser.checked) {
		//console.log("eraser selected");

		createjs.Touch.enable(stage);
	    createjs.Ticker.setFPS(24);

	    erasingCanvas = new createjs.Shape();

	    stage.addEventListener("stagemousedown", eraseMouseDown);
	    stage.addEventListener("stagemouseup", eraseMouseUp);
	    stage.removeChild(drawingCanvas);
    	stage.removeEventListener("stagemousedown", drawMouseDown);
	    stage.removeEventListener("stagemouseup", drawMouseUp);
	    stage.removeChild(crayonCanvas);
	    stage.removeEventListener("stagemousedown", crayonMouseDown);
	    stage.removeEventListener("stagemouseup", crayonMouseUp);

	    stage.addChild(erasingCanvas);
	    stage.update();
	}
}

function clearAll() {
	//console.log("canvas cleared");
	stage.removeChild(drawingCanvas);
	stage.removeChild(crayonCanvas);
	stage.removeChild(erasingCanvas);
	stage.clear();
	check();
}

function sizeChange(e) {
	stroke = e;
	//console.log("var stroke is: " + stroke + " and var e is: " + e);
	scaleTo = e/2;
	if(e == 2) {
		//console.log("e is equal to 2");
		TweenMax.to(sizeImg, 1, { css: { scaleX: 1, scaleY: 1} });
	}

	if(e > 2 && e <= 8) {
		//console.log("e is greater than 2 and less than 8");
		//console.log("e is " + e);
		//console.log(scaleTo);
		TweenMax.to(sizeImg, 1, { css: { scaleX: "1."+scaleTo, scaleY: "1."+scaleTo} });
	}

	if(e > 8 && e <= 18) {
		//console.log("e is greater than 8 and less than 18");
		//console.log(scaleTo);
		//console.log("e is " + e);
		TweenMax.to(sizeImg, 1, { css: { scaleX: "1."+scaleTo, scaleY: "1."+scaleTo} });
	}

	if(e > 18 && e <= 28) {
		//console.log("e is greater than 8 and less than 18");
		//console.log(scaleTo);
		minus = e-20;
		scaleTo = minus/2;
		//console.log("e is " + e);
		//console.log("minus: " + minus + " and scaleTo: " + scaleTo);
		TweenMax.to(sizeImg, 1, { css: { scaleX: "2."+scaleTo, scaleY: "2."+scaleTo} });
	}

	if(e == 30) {
		//console.log("e is greater than 8 and less than 18");
		//console.log(scaleTo);
		minus = e-20;
		scaleTo = minus/2;
		//console.log("e is " + e);
		//console.log("minus: " + minus + " and scaleTo: " + scaleTo);
		TweenMax.to(sizeImg, 1, { css: { scaleX: "2."+scaleTo, scaleY: "2."+scaleTo} });
	}
}

function changeColour(r, g, b) {
	//console.log("colour changed");
	//console.log(r, g, b);
	colour = "rgba("+r+","+g+","+b+",1)";
	//console.log(colour);
}

function drawMouseDown(event) {
	//console.log("drawing mouse down");
    oldPt = new createjs.Point(stage.mouseX, stage.mouseY);
    oldMidPt = oldPt;
    stage.addEventListener("stagemousemove" , drawMouseMove);
}

function drawMouseMove(event) {
	//console.log("drawing mouse move");
    var midPt = new createjs.Point(oldPt.x + stage.mouseX>>1, oldPt.y+stage.mouseY>>1);

    canvasCTX.globalCompositeOperation = "source-over";
    drawingCanvas.graphics.clear().setStrokeStyle(stroke, 'round', 'round').beginStroke(colour).moveTo(midPt.x, midPt.y).curveTo(oldPt.x, oldPt.y, oldMidPt.x, oldMidPt.y);
    canvasCTX.globalAlpha = 1;

    oldPt.x = stage.mouseX;
    oldPt.y = stage.mouseY;

    oldMidPt.x = midPt.x;
    oldMidPt.y = midPt.y;

    stage.update();
}

function drawMouseUp(event) {
	//console.log("drawing mouse up");
    stage.removeEventListener("stagemousemove" , drawMouseMove);
}

function crayonMouseDown(event) {
	//console.log("crayon mouse down");
    oldPt = new createjs.Point(stage.mouseX, stage.mouseY);
    oldMidPt = oldPt;
    stage.addEventListener("stagemousemove" , crayonMouseMove);
}

function crayonMouseMove(event) {
	//console.log("crayon mouse move");
    var midPt = new createjs.Point(oldPt.x + stage.mouseX>>1, oldPt.y+stage.mouseY>>1);

    canvasCTX.globalCompositeOperation = "source-atop";
    canvasCTX.strokeStyle = colour;
    canvasCTX.lineWidth = stroke;
    canvasCTX.stroke();
    canvasCTX.globalCompositeOperation = "source-over";

    crayonCanvas.graphics.clear().setStrokeStyle(stroke, 'square', 'square').beginBitmapStroke(crayonTextureImage).moveTo(midPt.x, midPt.y).curveTo(oldPt.x, oldPt.y, oldMidPt.x, oldMidPt.y);
    canvasCTX.globalAlpha = 0.4;

    oldPt.x = stage.mouseX;
    oldPt.y = stage.mouseY;

    oldMidPt.x = midPt.x;
    oldMidPt.y = midPt.y;

    stage.update();
}

function crayonMouseUp(event) {
	//console.log("crayon mouse up");
    stage.removeEventListener("stagemousemove" , crayonMouseMove);
}

function eraseMouseDown(event) {
	//console.log("erasing mouse down");
    oldPt = new createjs.Point(stage.mouseX, stage.mouseY);
    oldMidPt = oldPt;
    stage.addEventListener("stagemousemove" , eraseMouseMove);
}

function eraseMouseMove(event) {
	//console.log("erasing mouse move");
    var midPt = new createjs.Point(oldPt.x + stage.mouseX>>1, oldPt.y+stage.mouseY>>1);

    canvasCTX.globalCompositeOperation = "destination-out";
    erasingCanvas.graphics.clear().setStrokeStyle(stroke, 'round', 'round').beginStroke(colour).moveTo(midPt.x, midPt.y).curveTo(oldPt.x, oldPt.y, oldMidPt.x, oldMidPt.y);
    canvasCTX.globalAlpha = 1;

    oldPt.x = stage.mouseX;
    oldPt.y = stage.mouseY;

    oldMidPt.x = midPt.x;
    oldMidPt.y = midPt.y;

    stage.update();
}

function eraseMouseUp(event) {
	//console.log("erasing mouse up");
    stage.removeEventListener("stagemousemove" , eraseMouseMove);
}

function hideOpt() {
	TweenMax.to(hide, 1, { css: { opacity: 0} });
	TweenMax.to(show, 1, { css: { opacity: 1}, delay: 0.5 });
	TweenMax.to(options, 1, { css: { top: "-120px" } });

	hide.style.zIndex = 4;
	show.style.zIndex = 5;
}

function showOpt() {
	TweenMax.to(show, 1, { css: { opacity: 0} });
	TweenMax.to(hide, 1, { css: { opacity: 1}, delay: 0.5 });
	TweenMax.to(options, 1, { css: { top: "0px" } });

	show.style.zIndex = 4;
	hide.style.zIndex = 5;
}


window.addEventListener("load", init, false);

